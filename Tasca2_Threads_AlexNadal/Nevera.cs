﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasca2_Threads_AlexNadal
{
     class Nevera
    {
        public int _cerveses; 
        public Nevera(int cerveses)
        {
            _cerveses = cerveses;
        }
        public void OmplirNevera(string nom)
        {
            Random rnd = new Random();
            int cervesesAfegides = rnd.Next(0, 7);
            Console.WriteLine(nom + " ha afegit un total de " + cervesesAfegides + " cerveses a la nevera.");
            this._cerveses = Math.Min(9, this._cerveses + cervesesAfegides);
        }
        public void BeureCerveses(string nom)
        {
            Random rand = new Random();
            int cervesesBegudes = rand.Next(0, 7);
            Console.WriteLine(nom + " ha begut un total de " + cervesesBegudes + " cerveses de la nevera.");
            this._cerveses = Math.Max(0, this._cerveses - cervesesBegudes);
        }
    }
}
