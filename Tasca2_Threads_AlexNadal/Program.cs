﻿using Tasca2_Threads_AlexNadal;

internal class Program
{
    /*private static void Main(string[] args)
    {
        Nevera nevera = new Nevera(6);
        Thread threadAnitta = new Thread(() => nevera.OmplirNevera("Anitta"));
        Thread threadBadBunny = new Thread(() => nevera.BeureCerveses("Bad Bunny"));
        Thread threadLilNas = new Thread(() => nevera.BeureCerveses("Lil Nas"));
        Thread threadManuelTurizo = new Thread(() => nevera.BeureCerveses("Manuel Turizo"));

        threadAnitta.Start();
        threadAnitta.Join();
        Console.WriteLine("La nevera té un total de " + nevera._cerveses + " cerveses.");
        Console.WriteLine("");

        threadBadBunny.Start();
        threadBadBunny.Join();
        Console.WriteLine("La nevera té un total de " + nevera._cerveses + " cerveses.");
        Console.WriteLine("");

        threadLilNas.Start();
        threadLilNas.Join();
        Console.WriteLine("La nevera té un total de " + nevera._cerveses + " cerveses.");
        Console.WriteLine("");

        threadManuelTurizo.Start();
        threadManuelTurizo.Join();
        Console.WriteLine("La nevera té un total de " + nevera._cerveses + " cerveses.");
    }*/
    private static void Main(string[] args)
    {
        string text1 = "Una vegada hi havia un gat";
        string text2 = "En un lugar de la Mancha";
        string text3 = "Once upon a time in the west";

        Thread thread1 = new Thread(() => EscriuLent(text1));
        Thread thread2 = new Thread(() => EscriuLent(text2));
        Thread thread3 = new Thread(() => EscriuLent(text3));

        thread1.Start();
        thread1.Join();

        thread2.Start();
        thread2.Join();

        thread3.Start();
        thread3.Join();
    }
    public static void EscriuLent(string text)
    {
        string[] frase = text.Split(' ');

        for (int i = 0; i < frase.Length; i++)
        {
            Thread.Sleep(1000);
            Console.Write(frase[i] + " ");
        }
        Console.WriteLine();
    }
    
}